<?php

interface IUsuarioAlquilaProducto
{
    public function alquila(Producto $producto) : string;
}

class Usuario implements IUsuarioAlquilaProducto
{
    private $productoAlquilado = null;
	public function __construct(string $nombre,int $edad)
    {
        $this->nombre = $nombre;
        $this->edad = $edad;
    }

    public function UsuarioTieneAlquiler() : bool
    {
        return ($this->productoAlquilado) ? true : false;
    }
    public function alquila(Producto $producto) : string
    {
        if($this->UsuarioTieneAlquiler()){
            return 'Este usuario ya un producto alquilado';
        }

        $this->productoAlquilado = $producto;
        return "El usuario {$this->nombre} ha alquilado el {$this->productoAlquilado->tipo} : {$this->productoAlquilado->titulo}".PHP_EOL." precio  {$producto->precio}";
    }

	public function devuelve() : string
    {
        if ($this->productoAlquilado) {
            $out = "El usuario {$this->nombre} ha devuelto el {$this->productoAlquilado->tipo} : {$this->productoAlquilado->titulo}";
            $this->productoAlquilado = null;
            return $out;
        } else {
            return "El usuario {$this->nombre} no tiene ningún producto";
        }
    }
}

class Trabajador extends Usuario implements IUsuarioAlquilaProducto
{
    public function alquila(Producto $producto) : string
    {
		$precio = $producto->precio - ($producto->precio * 0.1);
        if($this->UsuarioTieneAlquiler()){
            return 'Este usuario ya un producto alquilado';
        }

        $this->productoAlquilado = $producto;
        return "El usuario {$this->nombre} ha alquilado el {$this->productoAlquilado->tipo} : {$this->productoAlquilado->titulo}".PHP_EOL." precio  $precio";
    }
}

abstract class Producto
{
    public string $tipo = 'Producto';
	public function __construct(string $titulo, ClasificacionPorEdad $clasificacionEdad = null)
    {
        $this->titulo = $titulo;
        $this->clasificacionEdad = $clasificacionEdad->nombreClasificacion;
    }
}

class Libro extends Producto
{
	public float $precio = 3.5;
    public string $tipo = 'Libro';

}

class Dvd extends Producto
{
	public float $precio = 5;
    public string $tipo = 'Dvd';
}


class ClasificacionPorEdad {

    public function __construct(string $nombreClasificacion,array $permisosClasificacion = [] ,int $edadMaxima,int $edadMinima)
    {

        $this->nombreClasificacion = $nombreClasificacion;
        $this->permisosClasificacion = (count($permisosClasificacion)>0) ? array_merge([$nombreClasificacion],$permisosClasificacion) : [$nombreClasificacion];
        $this->edadMaxima = $edadMaxima;
        $this->edadMinima = $edadMinima;
    }

    public function compruebaEdadUsuario(int $edad){
        return ($this->edadMinima <= $edad && $edad <= $this->edadMaxima);
    }
}

$ninos = new ClasificacionPorEdad('Niños',[],0,12 );
$jovenes = new ClasificacionPorEdad('Jóvenes',['Niños'],13,17 );
$adultos = new ClasificacionPorEdad('Adultos',['Jóvenes'],17,18 );


$usuario = new Trabajador('Pepe Reina',14);
$libro = new Libro('La isla del tesoro',$jovenes);

print $usuario->alquila($libro) . PHP_EOL;  // El usuario Pepe Reina ha alquilado el Libro : La isla del tesoro
print $usuario->devuelve() . PHP_EOL; // El usuario Pepe Reina ha devuelto el Libro : La isla del tesoro
print $usuario->devuelve() . PHP_EOL; // Pepe Reina no tiene ningún producto
?>
